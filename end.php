<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shocking sales 11.11</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="bg-gray-200">
    <div class="container p-5 text-center">
        <div class="mt-5 text-lg font-serif font-bold">
            HARAP MAAF MOD PEMBELIAN AWAL TELAH EXPIRED
        </div>
        <div class="mt-4">
            ANDA BOLEH MELAKUKAN PEMBELIAN PADA 11.11.2022
        </div>
    </div>

    <script>
        tailwind.config = {
            theme: {
                extend: {
                    colors: {
                        clifford: '#da373d',
                    }
                }
            }
        }

    </script>


</body>

</html>