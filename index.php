<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shocking sales 11.11</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="bg-gray-200">
    <div class="container p-5 text-center">
        <div class=" text-lg font-serif font-bold">
            INI ADALAH SALE LETTER
        </div>
        <div>PEMBELIAN AWAL AKAN DITUTUP DALAM <span id="time">03:00</span> MINIT!</div>
        <div>MASUKKAN PASSCODE</div>
        <div class="mt-3" id="form">
            <input type="text" name="passcode" id="passcode" class="text-center p-2" placeholder="NELOCO2022">
            <button class="p-2 px-3 bg-yellow-400" onclick="checkPasscode()">Submit</button>
        </div>
        <div id="order-button" class="text-center hidden">
            <div class="flex items-center justify-center p-12">
                <!-- Author: FormBold Team -->
                <!-- Learn More: https://formbold.com -->
                <div class="mx-auto w-full max-w-[550px]">
                    <form action="https://formbold.com/s/FORM_ID" method="POST">
                        <div class="-mx-3 flex flex-wrap">
                            <div class="w-full px-3 sm:w-1/2">
                                <div class="mb-5">
                                    <label for="fName" class="mb-3 block text-base font-medium text-[#07074D]">
                                        Nama
                                    </label>
                                    <input type="text" name="fName" id="fName" value="ALI BIN DAUD" placeholder="First Name" class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" />
                                </div>
                            </div>
                            <div class="w-full px-3 sm:w-1/2">
                                <div class="mb-5">
                                    <label for="lName" class="mb-3 block text-base font-medium text-[#07074D]">
                                        No Telefon
                                    </label>
                                    <input type="text" name="lName" id="lName" placeholder="Last Name" class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" value="<?=$_GET['phone']?>" />
                                </div>
                            </div>
                        </div>
                        <div class="-mx-3 flex flex-wrap">
                            <div class="w-full px-3 mb-5 ">
                                <label for="lName" class="mb-3 block text-base font-medium text-[#07074D]">
                                    Alamat
                                </label>
                                <input type="text" name="lName" id="lName" placeholder="Last Name" class="w-full rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" value="123, Jalan Bank, Sungai Petani, 08000 Sungai Petani, Kedah" />
                            </div>
                        </div>
                        <div class="mb-5">
                            <label for="guest" class="mb-3 block text-base font-medium text-[#07074D]"> 
                                JUMLAH PEMBELIAN

                            </label>
                            <input type="number" name="guest" id="guest" min="0" value="1" class="w-full appearance-none rounded-md border border-[#e0e0e0] bg-white py-3 px-6 text-base font-medium text-[#6B7280] outline-none focus:border-[#6A64F1] focus:shadow-md" />
                        </div>

                        <button class="p-4 px-6 bg-red-500 rounded-md shadow-md text-white w-full " id="order-button">BELI SEKARANG</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        tailwind.config = {
            theme: {
                extend: {
                    colors: {
                        clifford: '#da373d',
                    }
                }
            }
        }

        function startTimer(duration, display) {
            var timer = duration,
                minutes, seconds;
            setInterval(function() {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.textContent = minutes + ":" + seconds;

                if (--timer < 0) {
                    // timer = duration;
                    location.href = 'expired.php?phone=<?= $_GET['phone'] ?>';
                }
            }, 1000);
        }

        function checkPasscode() {
            let passcode = document.getElementById('passcode').value;
            if (passcode == 'NELOCO2022') {
                document.getElementById('form').style.display = 'none';
                document.getElementById('order-button').style.display = 'block';
            } else {
                alert('Sila masukkan passcode yang betul.')
            }
            var dur = 60 * 3,
                display = document.querySelector('#time');
            startTimer(dur, display);
            // console.log(passcode);
        }
    </script>


</body>

</html>